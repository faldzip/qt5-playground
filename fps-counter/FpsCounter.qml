import QtQuick 2.0

Item {
    id: fpsroot
    width: 128
    height: 64

    property int frameCount: 0

    Rectangle {
        id: spinner
        anchors.left: parent.left
        width: 24
        height: 24
        color: "transparent"

        RotationAnimation on rotation {
            from: 0; to: 360; duration: 1000; loops: Animation.Infinite
        }

        onRotationChanged: {
            fpsroot.frameCount++
        }
    }

    Text {
        id: fpsText
        anchors.right: fpsroot.right
        anchors.margins: 4
        font.pixelSize: fpsroot.height * 0.5
        font.family: "Courier"
        color: fps <= 15 ? "#FF0000" : (fps <= 30 ? "#FFFF00" : "#00FF00")
        text: fps.toFixed(1) + " fps"
        property real fps: 0
    }

    Timer {
        repeat: true
        running: true
        interval: 2000
        onTriggered: {
            fpsText.fps = fpsroot.frameCount / 2.0
            fpsroot.frameCount = 0;
        }
    }
}
