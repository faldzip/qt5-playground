import QtQuick 2.2
import "../fps-counter"

Item {
    width: 480
    height: 800

    Rectangle {
        id: bg
        anchors.fill: parent
        color: "black"
    }

    PathView {
        id: pathView
        anchors.fill: parent
        anchors.bottomMargin: 32
        model: 5
        pathItemCount: 2
        delegate: Item {
            id: droot
            width: pathView.width
            height: pathView.height
            opacity: PathView.pageOpacity
            transform: Rotation {
                angle: droot.PathView.pageAngle
                axis {
                    x: 0; y: 1; z: 0
                }
                origin {
                    x: droot.width * 0.5
                    y: droot.height * 0.5
                }
            }
            Rectangle {
                anchors.fill: parent
                anchors.margins: 16
                radius: 8
                color: "#80FFFFFF"
                opacity: 1.3 - droot.PathView.pageOpacity
            }
            Text {
                anchors.centerIn: parent
                color: "white"
                font.pixelSize: droot.height * 0.5
                text: index + 1
            }
        }

        path: Path {
            id: path
            startX: -pathView.width*0.1
            startY: pathView.height/2
            PathAttribute { name: "pageAngle"; value: 90 }
            PathAttribute { name: "pageOpacity"; value: 0.0 }
            PathLine { x: pathView.width*0.5; y: path.startY }
            PathAttribute { name: "pageAngle"; value: 0 }
            PathAttribute { name: "pageOpacity"; value: 1.0 }
            PathLine { x: pathView.width*1.1; y: path.startY }
            PathAttribute { name: "pageAngle"; value: -90 }
            PathAttribute { name: "pageOpacity"; value: 0.0 }
        }
        snapMode: PathView.SnapOneItem
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        highlightRangeMode: PathView.StrictlyEnforceRange
        Component.onCompleted: pathView.positionViewAtIndex(Math.floor(count * 0.5), PathView.Center)
    }

    Item {
        height: 48
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Row {
            height: 16
            width: pathView.count * 16
            anchors.centerIn: parent

            Repeater {
                model: pathView.count

                Item {
                    width: 16
                    height: 16
                    Rectangle {
                        property bool isSelected: index == pathView.currentIndex
                        width: parent.width * 0.75
                        height: parent.height * 0.75
                        anchors.centerIn: parent
                        color: "white"
                        radius: width * 0.5
                        opacity: isSelected ? 0.8 : 0.2
                        scale: isSelected ? 1.0 : 0.9

                        Behavior on opacity {
                            NumberAnimation { duration: 200 }
                        }
                        Behavior on scale {
                            NumberAnimation { duration: 200 }
                        }
                    }
                }
            }
        }
    }

    FpsCounter {
        anchors {
            right: parent.right
            top: parent.top
        }
    }
}

