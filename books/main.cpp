#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>
#include <QQmlEngine>
#include "booksreader.h"
#include "bookmodel.h"

#include <QtDebug>

QList<Book> loadBooks(const QString &filename)
{
    QFile file(filename);
    if (!file.open(QFile::ReadOnly|QFile::Text)) {
        qWarning("Cannot open xml file!");
        return QList<Book>();
    }
    BooksReader reader(&file);
    if (reader.readBooks())
        return reader.books();
    return QList<Book>();
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QList<Book> books = loadBooks("books.xml");
    qDebug() << books.count();
    BookModel model;
    model.setBooks(books);
    QQuickView view;
    view.rootContext()->setContextProperty("bookModel", &model);
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl("main.qml"));
    view.show();
    return app.exec();
}
