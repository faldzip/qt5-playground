#include "booksreader.h"

BooksReader::BooksReader(QIODevice *device)
{
    m_reader.setDevice(device);
}

bool BooksReader::readBooks()
{
    m_books.clear();
    if (m_reader.readNextStartElement() && tokenByName(m_reader.name()) == T_Catalog) {
        while (m_reader.readNextStartElement()) {
            if (tokenByName(m_reader.name()) == T_Book) {
                if (!readBook())
                    return false;
            }
        }
        return true;
    }
    return false;
}

BooksReader::Token BooksReader::tokenByName(const QStringRef &r)
{
    static QStringList tokens = QStringList() << "catalog" << "book" << "author" << "title"
                                              << "genre" << "price" << "publish_date" << "cover" << "description";
    return (Token)tokens.indexOf(r.toString());
}

bool BooksReader::readBook()
{
    if (tokenByName(m_reader.name()) != T_Book)
        return false;
    Book book;
    const QXmlStreamAttributes &attributes = m_reader.attributes();
    book.id = attributes.value("id").toString();

    while (m_reader.readNextStartElement()) {
        switch (tokenByName(m_reader.name())) {
        case T_Author:
            book.author = m_reader.readElementText();
            break;
        case T_Title:
            book.title = m_reader.readElementText();
            break;
        case T_Genre:
            book.genre = m_reader.readElementText();
            break;
        case T_Price:
            book.price = m_reader.readElementText().toDouble();
            break;
        case T_PublishDate:
            book.publishDate = QDate::fromString(m_reader.readElementText(), "yyyy-MM-dd");
            break;
        case T_Cover:
            book.cover = m_reader.readElementText();
            break;
        case T_Description:
            book.description = m_reader.readElementText().simplified();
            break;
        default:
            qWarning("Unsupported book element.");
        }
    }
    m_books.append(book);
    return true;
}
