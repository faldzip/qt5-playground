#include "bookmodel.h"

BookModel::BookModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

void BookModel::setBooks(const QList<Book> &books)
{
    beginResetModel();
    m_books = books;
    endResetModel();
}

int BookModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_books.count();
}

QVariant BookModel::data(const QModelIndex &index, int role) const
{
    int r = index.row();
    if (r < 0 || r >= m_books.count())
        return QVariant();
    switch (role) {
    case IdRole:
        return m_books.at(r).id;
    case AuthorRole:
        return m_books.at(r).author;
    case Qt::DisplayRole:
    case TitleRole:
        return m_books.at(r).title;
    case GenreRole:
        return m_books.at(r).genre;
    case PriceRole:
        return m_books.at(r).price;
    case PublishDateRole:
        return m_books.at(r).publishDate;
    case CoverRole:
        return m_books.at(r).cover;
    case DescriptionRole:
        return m_books.at(r).description;
    }
    return QVariant();
}

QHash<int, QByteArray> BookModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles.insert(IdRole, "bookId");
    roles.insert(AuthorRole, "author");
    roles.insert(TitleRole, "title");
    roles.insert(GenreRole, "genre");
    roles.insert(PriceRole, "price");
    roles.insert(PublishDateRole, "publishDate");
    roles.insert(CoverRole, "cover");
    roles.insert(DescriptionRole, "description");
    return roles;
}
