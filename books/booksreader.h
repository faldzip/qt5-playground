#ifndef BOOKSREADER_H
#define BOOKSREADER_H

#include <QXmlStreamReader>
#include "book.h"

class QIODevice;

class BooksReader
{
public:
    BooksReader(QIODevice *device);
    bool readBooks();
    inline QList<Book> books() const { return m_books; }
private:
    enum Token {
        T_Invalid = -1,
        T_Catalog,
        T_Book,
        T_Author,
        T_Title,
        T_Genre,
        T_Price,
        T_PublishDate,
        T_Cover,
        T_Description
    };

    static Token tokenByName(const QStringRef &r);

    bool readBook();

private:
    QXmlStreamReader m_reader;
    QList<Book> m_books;
};

#endif // BOOKSREADER_H
