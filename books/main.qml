import QtQuick 2.2
import QtQuick.Controls 1.1

Rectangle {
    width: 480
    height: 800
    color: "#202020"

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: ListView {
            id: listView
            model: bookModel
            delegate: Rectangle {
                width: listView.width
                height: 80
                color: index % 2 ? "#404040" : "#303030"

                Item {
                    anchors.fill: parent
                    anchors.margins: 8
                    Text {
                        anchors {
                            left: parent.left
                            right: parent.right
                            top: parent.top
                        }
                        height: parent.height/3
                        font.pixelSize: height * 0.75
                        font.italic: true
                        text: author
                        color: "white"
                    }
                    Text {
                        anchors {
                            left: parent.left
                            right: parent.right
                            bottom: parent.bottom
                        }
                        height: parent.height/3 * 2
                        font.pixelSize: height * 0.7
                        elide: Text.ElideRight
                        text: title
                        color: "white"
                    }
                }

                MouseArea {
                    id: ma
                    anchors.fill: parent
                    onClicked: {
                        stackView.push(detailsComp, {
                                           "author" : author,
                                           "title" : title,
                                           "genre" : genre,
                                           "publishDate" : publishDate.toLocaleDateString(),
                                           "price" : price,
                                           "cover" : Qt.resolvedUrl(cover),
                                           "description" : description }
                                       );
                    }
                }
            }
        }
    }

    Component {
        id: detailsComp
        BookDetails { }
    }
}
