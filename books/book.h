#ifndef BOOK_H
#define BOOK_H

#include <QString>
#include <QVariant>
#include <QDate>

struct Book
{
    QString id;
    QString author;
    QString title;
    QString genre;
    double price;
    QDate publishDate;
    QString cover;
    QString description;

    QVariant toVariant() const {
        QVariantMap result;
        result.insert("id", id);
        result.insert("author", author);
        result.insert("title", title);
        result.insert("genre", genre);
        result.insert("price", price);
        result.insert("publishDate", publishDate);
        result.insert("cover", cover);
        result.insert("description", description);
        return description;
    }
};

#endif // BOOK_H
