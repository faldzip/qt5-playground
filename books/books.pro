TEMPLATE = app
QT += xml quick qml
SOURCES += \
    main.cpp \
    booksreader.cpp \
    bookmodel.cpp

HEADERS += \
    booksreader.h \
    bookmodel.h \
    book.h

OTHER_FILES += \
    books.xml \
    main.qml \
    BookDetails.qml \
    Button.qml
