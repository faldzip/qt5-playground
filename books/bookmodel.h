#ifndef BOOKMODEL_H
#define BOOKMODEL_H

#include <QAbstractListModel>
#include "book.h"

class BookModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Role {
        IdRole = Qt::UserRole,
        AuthorRole,
        TitleRole,
        GenreRole,
        PriceRole,
        PublishDateRole,
        CoverRole,
        DescriptionRole
    };

    explicit BookModel(QObject *parent = 0);

    void setBooks(const QList<Book> &books);

    // QAbstractItemModel pure virtual functions implementation
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    // role names for QML
    QHash<int, QByteArray> roleNames() const;

private:
    QList<Book> m_books;
};

#endif // BOOKMODEL_H
