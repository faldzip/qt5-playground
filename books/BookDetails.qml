import QtQuick 2.2

Rectangle {
    width: 480
    height: 800
    color: "#303030"

    property alias author: authorTxt.text
    property alias title: titleText.text
    property alias genre: genreTxt.text
    property alias publishDate: pubdateTxt.text
    property alias price: priceTxt.text
    property alias cover: img.source
    property alias description: descText.text

    Image {
        id: img
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            margins: 16
        }
        height: parent.height * 0.35
        fillMode: Image.PreserveAspectFit
//        color: "transparent"
//        border.color: "white"
//        border.width: 2
    }

    Text {
        id: titleText
        anchors {
            top: img.bottom
            left: parent.left
            right: parent.right
        }
        height: parent.height * 0.15
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: height * 0.3
        font.bold: true
        wrapMode: Text.WordWrap
        elide: Text.ElideRight
        color: "white"
    }

    Grid {
        id: grid
        anchors {
            top: titleText.bottom
            horizontalCenter: parent.horizontalCenter
        }
        height: parent.height * 0.1
        columns: 2
        columnSpacing: 12

        property real textHeight: height * 0.25

        Text {
            height: parent.textHeight
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: height * 0.75
            color: "white"
            text: "Author:"
        }

        Text {
            id: authorTxt
            height: parent.textHeight
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: height * 0.75
            color: "white"
        }

        Text {
            height: parent.textHeight
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: height * 0.75
            color: "white"
            text: "Genre:"
        }

        Text {
            id: genreTxt
            height: parent.textHeight
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: height * 0.75
            color: "white"
        }

        Text {
            height: parent.textHeight
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: height * 0.75
            color: "white"
            text: "Publish date:"
        }

        Text {
            id: pubdateTxt
            height: parent.textHeight
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: height * 0.75
            color: "white"
        }

        Text {
            height: parent.textHeight
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: height * 0.75
            color: "white"
            text: "Price:"
        }

        Text {
            id: priceTxt
            height: parent.textHeight
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: height * 0.75
            color: "white"
        }
    }

    Text {
        id: descText
        anchors {
            left: parent.left
            right: parent.right
            top: grid.bottom
            bottom: parent.bottom
            margins: 24
        }
        color: "white"
        font.pixelSize: authorTxt.font.pixelSize
        verticalAlignment: Text.AlignTop
        horizontalAlignment: Text.AlignLeft
        wrapMode: Text.WordWrap
    }

    Button {
        anchors {
            left: parent.left
            bottom: parent.bottom
            margins: 24
        }
        height: parent.height * 0.1
        width: parent.width * 0.5 - anchors.margins * 1.5
        text: "Back"
        onClicked: stackView.pop()
    }

    Button {
        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: 24
        }
        height: parent.height * 0.1
        width: parent.width * 0.5 - anchors.margins * 1.5
        text: "Order"
    }
}
