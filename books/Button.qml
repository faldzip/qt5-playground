import QtQuick 2.2

Rectangle {
    property alias text: txt.text

    signal clicked

    radius: 8
    color: ma.pressed ? "steelblue" : "transparent"
    border.width: 2
    border.color: "white"
    
    Text {
        id: txt
        anchors.centerIn: parent
        font.pixelSize: parent.height * 0.5
        color: "white"
    }

    MouseArea {
        id: ma
        anchors.fill: parent
        hoverEnabled: true
        onClicked: parent.clicked()
    }
}
